var map;
var marker;
var jQueryLoaded = false;

$(function() {

	jQueryLoaded = true;
  
	$.get( 
		{
			url: "http://marsweather.ingenology.com/v1/latest/?format=jsonp",
			    
			// The name of the callback parameter
	    	jsonp: "callback",
	 
	    	// Tell jQuery we're expecting JSONP
	    	dataType: "jsonp",
	    
    		success: function( data ) {
	  			var result = data.report;

	  			$( "#terrestrial-date" ).html(result.terrestrial_date);
	  			$( "#marsian-date" ).html(result.sol);
	  			$( "#max-temp" ).html(result.max_temp);
	  			$( "#min-temp" ).html(result.min_temp);
	  			$( "#pressure" ).html(result.pressure);
	  			$( "#atmo-opacity" ).html(result.atmo_opacity);
	  		},

	  		error: function (error) {
	  			conosle.log(error);
	  		}
		}
	);

	$('#mars-map').on('load', function() {
	    $("#loading").hide();
		$("#tab-container").show();
		$("#mars-map-big").show();
	});

	$('#city-btn').click(function() {
		var city = $('#city-input').val();
		if (city != "") {
			$("#tab-container").hide();
			$("#mars-map-big").hide();
			$("#loading").show();
			$('#mars-map').attr("src", "https://www.wolframcloud.com/objects/user-55ede016-e048-4097-9b58-db218e02e037/MarsifyEarth?name=" + city);
			$("#mars-map-big").attr("src", "https://www.wolframcloud.com/objects/user-55ede016-e048-4097-9b58-db218e02e037/MarsMapNames?name=" + city);
			$('#mars-overlay').attr("src", "https://www.wolframcloud.com/objects/user-55ede016-e048-4097-9b58-db218e02e037/MarsifyEarthCoorOverlapNames?name=" + city);
			if (marker) {
	    		marker.setMap(null);
	    	}
		}
	})


});

function initMap() {
  	map = new google.maps.Map(document.getElementById('map'), {
    	center: {lat: 0.0, lng: 0.0},
    	zoom: 1
  	});

  	google.maps.event.addListener(map, 'click', function(event) {
    	console.log("", event.latLng);

    	if (marker) {
    		marker.setMap(null);
    	}

	    marker = new google.maps.Marker({
		    position: event.latLng,
		    map: map
		});


	    if (jQueryLoaded) {
	    	$('#city-input').val("");
		}

		updateImage(event.latLng.lat(), event.latLng.lng());

	});
}


function updateImage(lat, lng) {
	$("#tab-container").hide();
	$("#mars-map-big").hide();
	$("#loading").show();
	$('#mars-map').attr("src", "https://www.wolframcloud.com/objects/user-55ede016-e048-4097-9b58-db218e02e037/MarsifyEarthCoor?lat=" + lat + "&lon=" + lng);
	$('#mars-map-big').attr("src", "https://www.wolframcloud.com/objects/user-55ede016-e048-4097-9b58-db218e02e037/MarsMap?lat=" + lat + "&lon=" + lng);
	$('#mars-overlay').attr("src", "https://www.wolframcloud.com/objects/user-55ede016-e048-4097-9b58-db218e02e037/MarsifyEarthCoorOverlap?lat=" + lat + "&lon=" + lng);
}